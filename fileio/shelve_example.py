"""Shelve usage"""
import shelve

# Basic usage of shelve with fruits dictionary
with shelve.open("./fileio/shelve_test") as fruit:
    fruit["orange"] = "a sweet, orange, citrus fruit"
    fruit["apple"] = "great for making cider"
    fruit["lemon"] = "a sour, yellow citrus fruit"
    fruit["grape"] = "a small, sweet fruit growing in bunches"
    fruit["lime"] = "a sour, green citrus fruit"

    print(fruit["lemon"])
    print(fruit["grape"])

    fruit["lime"] = "great with tequilla"

    # List of fruits with keys
    print()
    print("List of fruits with keys:")
    for key in fruit:
        print(f"{key}: {fruit[key]}")

    # Sort items of shelve
    print()
    print("Sort items of shelve:")

    sorted_list = list(fruit.keys())
    sorted_list.sort()

    for item in sorted_list:
        print(f"{item}: {fruit[item]}")

    # Print all values of shelve
    print()
    print("Print all values of shelve:")

    for v in fruit.values():
        print(v)

    print(fruit.values())

    # Print all items of shelve
    print()
    print("Print all items of shelve:")

    for item in fruit.items():
        print(item)

    print(fruit.items())

    # Fruit Game
    print()
    print("Fruit Game:")
    while True:
        shelf_key = input("Please enter a fruit: ").lower()

        if shelf_key == "quit":
            break

        print(f"{fruit.get(shelf_key, f'We dont have a {shelf_key}')}")

print(fruit)

# Bike shelve usage
print()
print("Bike shelve usage:")

with shelve.open("./fileio/bike") as bike:
    bike["make"] = "Honda"
    bike["model"] = "250 Dream"
    bike["colour"] = "red"
    bike["engine_size"] = 250

    del bike["make"]

    for key in bike:
        print(key)

    print("=" * 30)

    print(bike["model"])
    print(bike["engine_size"])
    print(bike["colour"])
