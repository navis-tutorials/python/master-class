"""File Input Output"""
# Simple way to read a file
jabber = open("./fileio/sample.txt", "r")

for line in jabber:
    print(line)

jabber.close()

# Filtering text in the line
print()
print("Filtering text in the line:")

jabber = open("./fileio/sample.txt", "r")

for line in jabber:
    if "jabberwock" in line.lower():
        print(line, end="")  # end= will remove the new line character

jabber.close()

# Usage of with keyword
# It is a good practice to use with because if error occurs it will automatically close the file and show an error
print()
print("Usage of with keyword:")

with open("./fileio/sample.txt", "r") as jabber:
    for line in jabber:
        if "jabberwock" in line.lower():
            print(line, end="")

# Usage of readline() to read line
print()
print("Usage of readline() to read line:")

with open("./fileio/sample.txt", "r") as jabber:
    line = jabber.readline()
    while line:
        print(line, end="")
        line = jabber.readline()

# Usage of readlines() to read all lines at once
print()
print("Usage of readlines() to read all lines at once:")

with open("./fileio/sample.txt", "r") as jabber:
    lines = jabber.readlines()

print(lines)  # It is a list of lines

for line in lines:
    print(line, end="")

# Usage of read() to read all lines at once as string
print()
print("Usage of read() to read all lines at once as string:")

with open("./fileio/sample.txt", "r") as jabber:
    lines = jabber.read()

for line in lines[::-1]:
    print(line, end="")
