"""Writing text in a file module"""

CITIES_FILE_PATH = "./fileio/cities.txt"
IMELDA_FILE_PATH = "./fileio/imelda.txt"

# Writing data to a file
cities = ["Adelaide", "Alice Springs", "Darwin", "Melbourne", "Sydney"]

with open(CITIES_FILE_PATH, "w") as cities_file:
    for city in cities:
        print(city, file=cities_file)

# Reading data from the same file
print()
print("Reading data from the same file:")

cities = []

with open(CITIES_FILE_PATH, "r") as cities_file:
    for city in cities_file:
        cities.append(city.strip("\n"))  # Strips new line character while reading

print(cities)
for city in cities:
    print(city)

# Usage of eval() function
# It is not a good practice to use eval because it may make program unstabble if the string is virus
print()
print("Usage of eval() function:")

imelda = (
    "More Mayhem",
    "Imelda May",
    "2011",
    ((1, "Pulling the Rug"), (2, "Psycho"), (3, "Mayhem"), (4, "Kentish Town Waltz")),
)

# Write an object as it is in the file
print("Write an object as it is in the file:")
with open(IMELDA_FILE_PATH, "w") as imelda_file:
    print(imelda, file=imelda_file)

# Reading content from the file
print("Reading content from the file:")
with open(IMELDA_FILE_PATH, "r") as imelda_file:
    contents = imelda_file.readline()

imelda = eval(contents)
print(imelda)
title, artist, year, tracks = imelda
print(title)
print(artist)
print(year)
