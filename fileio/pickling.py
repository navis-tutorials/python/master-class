"""Pickle Library Usage Module"""
import pickle

imelda = (
    "More Mayhem",
    "Imelda May",
    "2011",
    ((1, "Pulling the Rug"), (2, "Psycho"), (3, "Mayhem"), (4, "Kentish Town Waltz")),
)
even = range(0, 10, 2)
odd = range(1, 10, 2)

with open("./fileio/imelda.pickle", "wb") as imelda_file:
    pickle.dump(imelda, imelda_file, protocol=pickle.HIGHEST_PROTOCOL)
    pickle.dump(even, imelda_file, protocol=0)
    pickle.dump(odd, imelda_file, protocol=pickle.DEFAULT_PROTOCOL)
    pickle.dump(2998302, imelda_file, protocol=pickle.DEFAULT_PROTOCOL)

with open("./fileio/imelda.pickle", "rb") as imelda_file:
    imelda_data = pickle.load(imelda_file)
    even_list = pickle.load(imelda_file)
    odd_list = pickle.load(imelda_file)
    x = pickle.load(imelda_file)

print(imelda_data)

album, artist, year, tracks = imelda_data
print(album)
print(artist)
print(year)

for track_number, track_title in tracks:
    print(track_number, track_title)

print("-" * 30)

for number in even_list:
    print(number)

print("-" * 30)

for number in odd_list:
    print(number)

print("-" * 30)

print(x)
