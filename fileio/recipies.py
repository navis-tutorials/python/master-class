"""Shelve append in detail"""
import shelve

blt = ["bacon", "lettuce", "tomato", "bread"]
beans_on_toast = ["beans", "bread"]
scrambled_eggs = ["eggs", "butter", "milk"]
soup = ["tin of soup"]
pasta = ["pasta", "cheese"]

with shelve.open("./fileio/recipes", writeback=True) as recipes:
    # Uncomment the below lines to insert data:

    # recipes["blt"] = blt
    # recipes["beans_on_toast"] = beans_on_toast
    # recipes["scrambled_eggs"] = scrambled_eggs
    # recipes["soup"] = soup
    # recipes["pasta"] = pasta

    # Normal update append will not work because by default shelve stores data in memory
    # not in the file

    # recipes["blt"].append("butter")
    # recipes["pasta"].append("tomato")

    # Approach 1 to update or append data

    # temp_list = recipes["blt"]
    # temp_list.append("butter")
    # recipes["blt"] = temp_list

    # temp_list = recipes["pasta"]
    # temp_list.append("tomato")
    # recipes["pasta"] = temp_list

    # Approach 2 add writeback in open function and append the data as normal append
    # Disadvantage of this approach is it needs more memory because it will write data at same time

    recipes["soup"].append("croutons")

    for snack in recipes:
        print(f"{snack}: {recipes[snack]}")
