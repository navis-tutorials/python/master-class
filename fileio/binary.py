"""Writing and Reading Binary Data Module"""

BINARY_FILE_PATH = "./fileio/binary"

with open(BINARY_FILE_PATH, "bw") as bin_file:
    for i in range(17):
        bin_file.write(bytes([i]))

with open(BINARY_FILE_PATH, "br") as binary_file:
    for b in binary_file:
        print(b)
