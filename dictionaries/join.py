"""Join is best approach than concatenation"""
# Here is a simple approach to concate every element in the for loop
# Disadvantage is string is immutable, and it will create a new variable every time
my_list = ["a", "b", "c", "d"]

new_string = ""
for char in my_list:
    new_string += char + ", "

print(new_string)

# Join is best option in comparision to above
print()
print("Join is best option in comparision to above:")

new_string = ", ".join(my_list)
print(new_string)


# Write a program to allow user to visit locations according to the map
# Please refer to 'cave_map.jpg' in the same folder

print()
print("Program for a cave map:")
locations = {
    0: "You are sitting in a front of computer learning Python",
    1: "You are standing at the end of a road before a small brick building",
    2: "You are at the top of a hill",
    3: "You are inside a building, a well house for a small stream",
    4: "You are in a valley beside a stream",
    5: "You are in the forest",
}

exits = [
    {"Q": 0},
    {"W": 2, "E": 3, "N": 5, "S": 4, "Q": 0},
    {"N": 5, "Q": 0},
    {"W": 1, "Q": 0},
    {"N": 1, "W": 2, "Q": 0},
    {"W": 2, "S": 1, "Q": 0},
]

loc = 1
while True:
    available_exits = ", ".join(exits[loc].keys())

    print(locations[loc])

    if loc == 0:
        break

    direction = input(f"Available exits are {available_exits}: ").upper()
    print()

    if direction in exits[loc]:
        loc = exits[loc][direction]
    else:
        print("You can't go in this direction")
