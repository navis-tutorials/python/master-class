"""More methods of Dictionary"""

fruit = {
    "orange": "a sweet, orange citrus fruit",
    "apple": "good for making cider",
    "lemon": "a sour, yellow citrus fruit",
    "grape": "a small, sweet fruit growing in bunches",
    "lime": "a sour, green citrus fruit",
}

veg = {
    "cabbage": "every child's favourite",
    "sprouts": "mmmmm, lovely",
    "spinach": "can I have some more fruits, please",
}

print(fruit)
print(veg)

# Use update() to combine two dictionaries
print()
print("Use update() to combine two dictionaries:")

fruit.update(veg)
print(fruit)
print(veg)

# Use copy() to create a clone of dictionary
print()
print("Use copy() to create a clone of dictionary:")

nice_and_nasty = veg.copy()
nice_and_nasty.update(fruit)
print(nice_and_nasty)
print(fruit)
print(veg)
