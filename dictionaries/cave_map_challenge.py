"""Cave Map Challenge Module"""

# Modify the program so that the exits is a dictionary rather than a list,
# with the keys being the numbers of the locations and
# the values being dictionaries holding the exits (as they do at present).
# No change should be needed to the actual code.
#
# Once that is created, create another dictionary that contains letters that players may use.
# These words will be the keys, and
# their value will be a single letter that the program can use to determine which way to go.

locations = {
    0: "You are sitting in a front of computer learning Python",
    1: "You are standing at the end of a road before a small brick building",
    2: "You are at the top of a hill",
    3: "You are inside a building, a well house for a small stream",
    4: "You are in a valley beside a stream",
    5: "You are in the forest",
}

exits = {
    0: {"Q": 0},
    1: {"W": 2, "E": 3, "N": 5, "S": 4, "Q": 0},
    2: {"N": 5, "Q": 0},
    3: {"W": 1, "Q": 0},
    4: {"N": 1, "W": 2, "Q": 0},
    5: {"W": 2, "S": 1, "Q": 0},
}

named_exits = {
    1: {"2": 2, "3": 3, "5": 5, "4": 4},
    2: {"5": 5},
    3: {"1": 1},
    4: {"1": 1, "2": 2},
    5: {"2": 2, "1": 1},
}

vocabulary = {
    "WEST": "W",
    "EAST": "E",
    "NORTH": "N",
    "SOUTH": "S",
    "QUIT": "Q",
    "ROAD": "1",
    "HILL": "2",
    "BUILDING": "3",
    "VALLEY": "4",
    "FOREST": "5",
}

loc = 1
while True:
    available_exits = ", ".join(exits[loc].keys())

    print(locations[loc])

    if loc == 0:
        break
    else:  # Usage of copy() and update() method of dictionary
        all_exits = exits[loc].copy()
        all_exits.update(named_exits[loc])

    direction = input(f"Available exits are {available_exits}: ").upper()
    print()

    if len(direction) > 1:  # It means it is a word
        direction_words = direction.split()

        for [word, value] in vocabulary.items():
            if word in direction_words:
                direction = vocabulary[word]

    if direction in all_exits:
        loc = all_exits[direction]
    else:
        print("You can't go in this direction")
