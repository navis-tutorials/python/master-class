"""Dictonaries"""

fruit = {
    "orange": "a sweet, orange citrus fruit",
    "apple": "good for making cider",
    "lemon": "a sour, yellow citrus fruit",
    "grape": "a small, sweet fruit growing in bunches",
    "lime": "a sour, green citrus fruit",
    "apple": "round and crunchy",  # It will over write the apple key
}

print("Print the dictionary as it is:")
print(fruit)

print()
print("Access dictionary element by using key:")
print(fruit["apple"])

print()
print("Add a new entry in the existing dictionary")
fruit["pear"] = "an odd shaped apple"
print(fruit)

print()
print("Override an existing entry:")
fruit["lime"] = "best with tequilla"

print()
print("Delete an entry from the dictionary")
del fruit["orange"]
print(fruit)

print()
print("Sorting of dictionary:")

for key in sorted(fruit.keys()):
    print(f"`{key}` in `{fruit[key]}`")

print()
print("Another way of sorting a dictionary:")

ordered_keys = list(fruit.keys())
ordered_keys.sort()

for key in ordered_keys:
    print(f"`{key}` in `{fruit[key]}`")

print()
print("Get all keys and values of a dictionary:")
print(fruit.keys())
print(fruit.values())

print()
print("Dynamic view object of dictionary `keys()` that update var automatically:")
fruit_keys = fruit.keys()
print(fruit_keys)

fruit["tomato"] = "not nice with ice cream"
print(fruit_keys)

print()
print("Another dynamic view object of dictanary `items()`:")

print(fruit.items())
f_tuple = tuple(fruit.items())
print(f_tuple)

for snack in f_tuple:
    item, description = snack
    print(f"{item} in {description}")

print()
print("Convert tuple to dictionary:")
print(dict(f_tuple))

print()
print("Usage of get method:")

while True:
    dict_key = input("Please enter a fruit: ")

    if dict_key == "quit":
        break

    # Another way to provide a default value if the key is not found
    description = fruit.get(dict_key, f"We don't have a {dict_key}")
    print(description)

    # if dict_key in fruit:
    #     description = fruit.get(dict_key)
    #     print(description)
    # else:
    #     print(f"We don't have a {dict_key}")

print()
print("Clear all entries of the dictionary")
fruit.clear()
print(fruit)
