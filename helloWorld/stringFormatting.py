# String formatting
print("String formatting:")

# Simple for loop w/o formatting
print()
print("Simple for loop w/o formatting")
for i in range(1, 13):
    print("No. {0} squared is {1} and cubed is {2}".format(i, i ** 2, i ** 3))

# Same for loop with formatting by default it is right aligned
print()
print("Same for loop with formatting")
for i in range(1, 13):
    print("No. {0:2} squared is {1:3} and cubed is {2:4}".format(i, i ** 2, i ** 3))

# Left Alignment
print()
print("Same for loop with formatting, and set left align")
for i in range(1, 13):
    print("No. {0:2} squared is {1:<3} and cubed is {2:<4}".format(i, i ** 2, i ** 3))

# Center Alignment
print()
print("Same for loop with formatting, and center align")
for i in range(1, 13):
    print("No. {0:2} squared is {1:^3} and cubed is {2:^4}".format(i, i ** 2, i ** 3))

# Working with float numbers
print()
print("Working with float numbers:")

# By default it add 15 digits precision, if we don't mention any precision
print("Pi is approximately {0:12}".format(22 / 7))

# In this case, it will divide the format width equally i.e. 6 + 6
print("Pi is approximately {0:12f}".format(22 / 7))

# In this case, it will not truncate the precision because it is more important than the format width
print("Pi is approximately {0:12.50f}".format(22 / 7))

# It will show the same output as before because there are two digits before the precision, the first is 3, and the second is "."
print("Pi is approximately {0:52.50f}".format(22 / 7))

# It will increase the format width, and move the output to the right hand side
print("Pi is approximately {0:62.50f}".format(22 / 7))

# It will also increase the format width, but due left align it will not show any white-space
print("Pi is approximately {0:<72.50f}".format(22 / 7))

# Python show maximum of 50 digits precision, and after this it will show zeros starting with 5
print("Pi is approximately {0:<72.54f}".format(22 / 7))

# Python show maximum of 50 digits precision, and after this it will show zeros starting with 5
print("Pi is approximately {0:<72.64f}".format(22 / 7))

# We can also omit the replacement field index and Python will automatically replace the strings
print()
print("Omit the replacement field index:")
for i in range(1, 13):
    print("No. {} squared is {} and cubed is {:4}".format(i, i ** 2, i ** 3))

# f-string function is introduced in version 3.6, so use it wisely
print()
print("f-string function usage:")

print(f"Pi is approximately {22/7:52.50f}")

# We can also use variables with f-string function
pi = 22 / 7
print(f"Pi is approximately {pi:52.50f}")
