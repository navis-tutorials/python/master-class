name = "Navi Singh"
print(name)
print(type(name))

age = 32
print(age)
print(type(age))

# Python believes in binding or attaching variable to memory not to the type
age = "32 years"
print(age)
print(type(age))

# Python is strongly typed language and don't allow mixing of different type of variables
print(name + 34)  # It will raise an error
