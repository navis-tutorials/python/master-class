print("Today is a good day to learn Python")
# fmt: off
print('Python is fun')
# fmt: on
print("Python's strings are easy to use")
print('We can even include "quotes" in strings')
print("Hello" + " World")

greeting = "Hello"
fname = "Navi"
lname = input("Please enter your last name: ")

# if we want a space, we can add that too
print(greeting + " " + fname + " " + lname)
