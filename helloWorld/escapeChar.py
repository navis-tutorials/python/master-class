split_string = "This string has been \nsplit over\nseveral\nlines"
print(split_string)

tabbed_string = "1\t2\t3\t4"
print(tabbed_string)

# fmt: off
esc_single_quote = 'The pet shop owner said "No, no, \'e\'s uh,...he\'s resting."'
print(esc_single_quote)

# fmt: on
esc_double_quote = "The pet shop owner said \"No, no, 'e's uh,...he's resting.\""
print(esc_double_quote)

three_quotes_string = """The pet shop owner said "No, no, 'e's uh,...he's resting." """
print(three_quotes_string)

another_split_string = """This string has been
split over
several
lines"""
print(another_split_string)

# Add backslashes at the end of each to remove new line
not_spliting_string = """This string has been \
split over \
several \
lines"""
print(not_spliting_string)

# Ways to show backslash in a string
print("C:\\Users\\Navi Singh\\python.py")  # Escape backslash with another backslash
print(r"C:\Users\Navi Singh\python.py")  # Add rule string character at the beginning
