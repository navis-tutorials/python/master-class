# Slicing Backwards
print("Slicing Backwards")

letters = "abcdefghijklmnopqrstuvwxyz"

# It will print the string in reverse order but will ignore the alphabet "a" because of end point
print(letters[25:0:-1])

# It will print the string in reverse order
print(letters[25::-1])

# Another way to print the string in reverse order
print(letters[::-1])

# Challenge - Use the above letters string, add some code to create the following slices.
# 1. Create a slice that produces the characters qpo.
# 2. Slice the string to produce edcba.
# 3. Slice the string to produce the last 8 characters, in reverse order.

# My Answer for challenge 1
print()
print("My answer for Challenge 1")
print(letters[16:13:-1])

# My Answer for challenge 2
print()
print("My answer for Challenge 2")
print(letters[4::-1])

# My Answer for challenge 3
print()
print("My answer for Challenge 3")
print(letters[25:17:-1])

# Acutual answer for challenge 1 was same as my answer
# Acutual answer for challenge 2 was same as my answer
# Acutual answer for challenge 3
print(letters[:-9:-1])

# Some Slicing Idioms
print()
print("Some Slicing Idioms")

# It will print last four characters
print(letters[-4:])

# It will print last character
print(letters[-1:])

# It will print first character
print(letters[:1])

# It will print first character
print(letters[0])
