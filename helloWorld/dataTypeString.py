print("String Data Type")

#                   11111
#         012345678901234
parrot = "Norwegian Blue"

# Simply print the string variable
print(parrot)

# Print a specific character at postion 10 in the string
print(parrot[10])

# Mini Challenge
print()
print("Mini Challenge")

# Add some code to the program, so that it prints out "we win."
# Each character should appear on a separate line.
# The program should get the characters from the parrot string, using indexing.
# The final output from the program should be:
# w
# e
#
# w
# i
# n
print()
print("My Answer")
print(
    parrot[3]
    + "\n"
    + parrot[4]
    + "\n\n"
    + parrot[3]
    + "\n"
    + parrot[6]
    + "\n"
    + parrot[8]
)

# Actual answer was
print()
print("Actual answer was")
print(parrot[3])
print(parrot[4])
print(parrot[9])
print(parrot[3])
print(parrot[6])
print(parrot[8])

# Negative Indexing in Strings
print()
print("Negative Indexing in Strings")

# It will print the last character in the string
print(parrot[-1])

# Follow the last challenge, but this time use Negative Indexing
print()
print("Same previous challenge with negative indexing")
print(parrot[-11])
print(parrot[-10])
print(parrot[-5])
print(parrot[-11])
print(parrot[-8])
print(parrot[-6])

# Another way is to just minus the positive index from the total number of characters
# In this case we have a total of 14 characters in the parrot variable
print()
print(
    "Another way is to just minus the positive index from the total number of characters"
)
print(parrot[3 - 14])
print(parrot[4 - 14])
print(parrot[9 - 14])
print(parrot[3 - 14])
print(parrot[6 - 14])
print(parrot[8 - 14])

# Slicing of string
print()
print("String Slicing")
print(parrot[0:6])  # Norweg
print(parrot[3:5])  # we
print(parrot[0:9])  # Norwegian

# Another way to get same result as above, because the start defaults to the beginning of the sequence
# * The colon is necessary, otherwise we'd be specifying the single character at position 9
print(parrot[:9])

# Challenge to get "Blue" by using slicing
print()
print('Challenge to get "Blue" by using slicing')
print(parrot[10:14])  # Blue

# Another way to print the last one is to exlude the end index but colon in required
print()
print("Another way to print the same as above")
print(parrot[10:])  # Blue

# Both the start and the end parameters are optional, and when they omitted then it will print the whole string
print()
print("Omitted both the start and the end")
print(parrot[:])

# Slicing with Negative numbers
print()
print("Slicing with Negative numbers")

print(parrot[-4:-2])  # Bl
print(parrot[-4:12])  # Bl
print(parrot[-14:-8])  # Norweg
print(parrot[-4:2])  # It will print nothing because you can't go from start to backward

# Using a step in a Slice
print()
print("Using a step in a slice")

print(parrot[0:6:2])  # Nre
print(parrot[0:6:3])  # Nw

numberStr = "9,223;372:036 854,775;807"
numSeperators = numberStr[1::4]
print(numSeperators)

numValues = "".join(
    char if char not in numSeperators else " " for char in numberStr
).split()
print([int(val) for val in numValues])
