a = 12
b = 3

print(a + b)  # 15
print(a - b)  # 9
print(a * b)  # 36
print(a / b)  # 4.0
print(a // b)  # 4 integer division, rounded down to minus infinity
print(a % b)  # 0 modulo: the remainder after integer division

# Empty line
print()

# It will print numbers from 1 to 3
for i in range(1, a // b):
    print(i)

# Operator Precedence in Python
# PEMDAS - Parentheses, Exponents, Multiplication/Division, Addition/Subtraction
# BEDMAS - Brackets, Exponents, Division/Multiplication, Addition/Subtraction
# BODMAS - Brackets, Order, Division/Multiplication, Addition/Subtraction
# BIDMAS - Brackets, Index, Division/Multiplication, Addition/Subtraction
# * Multiplication and Division have equal precedence
# * Addition and Subtraction also have equal precedence

# In this example it give precedence to / and *
print(a + b / 3 - 4 * 12)

# Same as above, an example to show that how it works by default
print(a + (b / 3) - (4 * 12))

# Add parentheses to get the expected result as in the first example
print((((a + b) / 3) - 4) * 12)

# Rewrite of previous example as per precendence
print(((a + b) / 3 - 4) * 12)

# Rewrite the previous example by using variables
addition = a + b
division = addition / 3
subtraction = division - 4
multiplication = subtraction * 12
print(multiplication)

# In an expression that mixes operations with equal precedence, they're evaluated from left to right
print(a / b * a / b)
