# String Replacement Fields replace integer type value with String type value
print("String Replacement Fields:")

# Coerce Numbers into a String, using the str function
print("Coerce Numbers into a String, using the str function:")
age = 32
print("My age is " + str(age) + " years.")

# Usage of string replacement
print()
print("Usage of string replacement")
print("My age is {0} years.".format(age))

# More usages of string replacement
print()
print("More usages of string replacement:")

print()
print(
    "There are {0} days in {1}, {2}, {3}, {4}, {5}, {6} and {7}".format(
        31, "Jan", "Mar", "May", "Jul", "Aug", "Oct", "Dec"
    )
)

print()
print(
    "Jan: {2}, Feb: {0}, Mar: {2}, Apr: {1}, May: {2}, Jun: {1}, Jul: {2}, Aug: {2}, Sep: {1}, Oct: {2}, Nov: {1}, Dec: {2}".format(
        28, 30, 31
    )
)

print()
print(
    """Jan: {2}
Feb: {0}
Mar: {2}
Apr: {1}
May: {2}
Jun: {1}
Jul: {2}
Aug: {2}
Sep: {1}
Oct: {2}
Nov: {1}
Dec: {2}""".format(
        28, 30, 31
    )
)
