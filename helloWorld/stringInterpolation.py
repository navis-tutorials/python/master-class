# It is an old way to format or interpolate the string, and was used in the version 2. In the version 3 it is deprecated, and soon it will be get removed. Please use this method only in the version 2.
print("String interpolation:")

# A variable is used for interpolation
print()
print("Simple way to use a variable:")
age = 32
print("My age is %d years." % age)

# We can use different variables too
print()
print("We can use different variables too:")
major = "years"
minor = "months"
print("My age is %d %s and %d %s." % (age, major, 6, minor))

# Interpolation for float type numbers
print()
print("Interpolation for float type numbers:")
print("Pi is approximately %f" % (22 / 7))
print("Pi is approximately %55.50f" % (22 / 7))
