# Operators that are used for sequence
print("Operators that are used for sequence:")

string1 = "he's "
string2 = "probably "
string3 = "pining "
string4 = "for the "
string5 = "fjords"

print()
print("Strings can be concatenate:")

# Concatenate the string variables to print the sentence
print(string1 + string2 + string3 + string4 + string5)

# + operator is not necessary while concatenation of strings only
print("he's " "probably " "pining " "for the " "fjords")


# String can be multiplied
print()
print("Strings can be multiplied:")

# It will print Navi 5 times
print("Navi " * 5)

# Operators precedence will be the same as discussed before
print()
print("Operators precedence:")

print("Navi " * (5 + 4))
print("Navi " * 5 + "4")

# Check the substring existence in the given string
print()
print("Check the substring existence in the given string:")

today = "Monday"
print("day" in today)  # True
print("Mon" in today)  # True
print("mon" in today)  # False because it is case-sensitive
print("Sun" in today)  # False
print("Wed" in "fjords")  # False
