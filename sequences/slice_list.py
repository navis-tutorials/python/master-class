computer_parts = ["computer", "monitor", "keyboard", "mouse", "mouse mat"]
print(computer_parts)  # ['computer', 'monitor', 'keyboard', 'mouse', 'mouse mat']

# Replace a particular index of a list
computer_parts[3] = "trackball"
print(computer_parts)  # ['computer', 'monitor', 'keyboard', 'trackball', 'mouse mat']

# Replace the content of the list from i to j with an iterable on the right hand side
computer_parts[3:4] = ["mouse", "trackball"]
print(
    computer_parts
)  # ['computer', 'monitor', 'keyboard', 'mouse', 'trackball', 'mouse mat']

computer_parts[3:] = ["trackball"]
print(computer_parts)  # ['computer', 'monitor', 'keyboard', 'trackball']
