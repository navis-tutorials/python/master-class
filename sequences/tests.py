# 1. Edge Case
# 2. Corner Case

# We should test our code with data that meets the following criteria:
# 1. Outlying the values at both the low and high ends.
# 2. Outlying the values at the low end only.
# 3. Outlying the values at the high end only.
# 4. No outlying values.
# 5. Only outlying values (no valid ones).
# 6. An empty data set.

# Test 1
print("Test 1")
data = [
    4,
    5,
    104,
    105,
    110,
    150,
    185,
    350,
    360,
]

# Test 2
# print("Test 2")
# data = [
#     4,
#     5,
#     104,
#     105,
#     110,
#     150,
#     185,
# ]

# Test 3
# print("Test 3")
# data = [
#     104,
#     105,
#     110,
#     150,
#     185,
#     350,
#     360,
# ]

# Test 4
# print("Test 4")
# data = [
#     104,
#     105,
#     110,
#     150,
#     185,
# ]

# Test 5
# print("Test 5")
# data = [
#     1104,
#     1105,
#     1110,
#     1150,
#     1185,
#     1350,
#     1360,
# ]

# Test 6
# print("Test 6")
# data = []

min_valid = 100
max_valid = 200

print(data)

# Process the low values in the list
print()
print("Process the low values in the list:")

stop = 0
for index, value in enumerate(data):
    if value >= min_valid:
        stop = index
        break

print(stop)
del data[:stop]
print(data)

# Process the high values in the list
print()
print("Process the high values in the list:")

start = 0
for index in range(len(data) - 1, -1, -1):
    if data[index] <= max_valid:
        start = index + 1
        break

print(start)
del data[start:]
print(data)
