# Short hand way to unpack tuple in the condition
print("Short hand way to unpack tuple in the condition:")
for index, value in enumerate("abcdefgh"):
    print(index, value)

# Unpacking tuple in the definition of for
print()
print("Unpacking tuple in the definition of for:")
for t in enumerate("abcdefgh"):
    index, value = t
    print(index, value)
