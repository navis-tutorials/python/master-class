# General way to declare a tuple
print("General way to declare a tuple:")

a = ("a", "b", "c")
print(a)

# Access tuple elements through index just like a list
print()
print("Access tuple elements through index just like a list:")

metallica = "Ride the Lightning", "Metallica", 1984

print(metallica)
print(metallica[0])
print(metallica[1])
print(metallica[2])

# Tuples are immutable which means we can't change the values of a tuple
print()
print("Tuples are immutable which means we can't change the values of a tuple:")

# metallica[0] = "Master of Puppets"

# On the other hand, Lists are mutable that means we can change the values of a list
print()
print(
    "On the other hand, Lists are mutable that means we can change the values of a list:"
)

metallica_list = list(metallica)
print(metallica_list)

metallica_list[0] = "Master of Puppets"
print(metallica_list)

# Why should we use unpacking

# Example 1
print()
print("Example 1:")

title, artist, year = metallica
print(title)
print(artist)
print(year)

# Example 2
print()
print("Example 2:")

table = ("Coffee table", 200, 100, 75, 34.50)
print("Using index of table:", table[1] * table[2])

name, length, width, height, price = table
print("Using unpacked vars:", length * width)

# Nested tuples in a list
print()
print("Nested tuples in a list:")

albums = [
    ("Welcome to my Nightmare", "Alice Cooper", 1975),
    ("Bad Company", "Bad Company", 1974),
    ("Nightflight", "Budgie", 1981),
    ("More Mayhem", "Emilda May", 2011),
    ("Ride the Lightning", "Metallica", 1984),
]

print(len(albums))

# Mini Challenge
# Add unpakcing code for the tuple
print()
print("My answer to the challenge:")
for name, artist, year in albums:
    print("Album: {}, Artist: {}, Year: {}".format(name, artist, year))

# Actual answer is same as above
