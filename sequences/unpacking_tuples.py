# Bound several variables to a single value
print("Bound several variables to a single value:")
a = b = c = d = e = f = 42
print(c)

# Unpacking a tuple
print()
print("Unpacking a tuple:")

x, y, z = 10, 20, 30

print(x)
print(y)
print(z)

# Another way of unpacking a tuple varible
print()
print("Another way of unpacking a tuple varible:")

data = 10, 20, 30  # data represents a tuple
x, y, z = data

print(x)
print(y)
print(z)

# Risk of unpacking a sequence other than tuple
print()
print("Risk of unpacking a sequence other than tuple:")

data = [100, 200, 300]  # data represents a list
# data.append(400) # It will crash the program because new value is added
x, y, z = data

print(x)
print(y)
print(z)

# As a result, tuples are reliable for unpacking because they are immutable
