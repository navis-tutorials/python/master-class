print("True boolean id:")
result = True
another_result = result
result_true = True
print(id(result))
print(id(another_result))
print(id(result_true))

print()
print("False boolean id:")
result = False
another_result = result
result_false = False
print(id(result))
print(id(another_result))
print(id(result_false))

print()
print("String id:")
result = "Correct"
another_result = result
print(id(result))
print(id(another_result))

result += "ish"
print(id(result))
print(id(another_result))
