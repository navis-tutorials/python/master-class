shopping_list = ["milk", "pasta", "eggs", "spam", "bread", "rice"]
another_list = shopping_list

print(id(shopping_list))
print(id(another_list))

shopping_list += ["cookies"]
print(shopping_list)
print(id(shopping_list))

# Binding multiple names to a List
print()
print("Binding multiple names to a list")

print(another_list)

a = b = c = d = e = f = another_list

print(a)

print("Adding cream")
b.append("Cream")
print(c)
print(d)
print(e)
print(f)
print(another_list)
