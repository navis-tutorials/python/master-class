flowers = [
    "Daffodil",
    "Evening Primrose",
    "Hydrangea",
    "Iris",
    "Lavender",
    "Sunflower",
    "Tiger Lily",
]

seperator = ", "
output = seperator.join(flowers)

print(output)

print(",".join(flowers))
