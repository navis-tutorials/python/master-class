from albums_data import albums

SONGS_INDEX = 3
SONG_TITLE_INDEX = 1

while True:
    print("Please choose your album (invalid choice exits):")

    for index, (title, artist, year, songs) in enumerate(albums):
        print("{}: {}".format(index + 1, title))

    choice = int(input())
    if 1 <= choice <= len(albums):
        songs_list = albums[choice - 1][SONGS_INDEX]
    else:
        break

    print("Please choose your song:")

    for index, (track_number, song) in enumerate(songs_list):
        print("{}: {}".format(index + 1, song))

    song_choice = int(input())
    if 1 <= song_choice <= len(songs_list):
        title = songs_list[song_choice - 1][SONG_TITLE_INDEX]

        print()
        print("Playing {}".format(title))

    print()
    print("=" * 48)
    print()

# Mini Challenge
# At the moment, if you enter an invalid choice for a song, the program terminates.
# That's fine when the albums are being displayed, but we want slight different behaviour when the user's choosing a song.
# Instead of exiting the program, an invalid song choice should dispaly the list of albums again.
# That will allow user to go back to the albums, without choosing a song to play.

# Actual answer is little bit different. Please check above.
