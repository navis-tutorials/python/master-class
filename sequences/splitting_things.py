# Normal string splitting
print("Normal string splitting:")
panagram = "The quick brown fox jumps over the lazy dog"

words = panagram.split()
print(words)

# String splitting contains one or more spaces
print()
print("String splitting contains one or more spaces:")
panagram = """The quick brown
fox jumps\tover
the lazy dog"""

words = panagram.split()
print(words)

# Use seprator in split method
print()
print("Use seprator in split method:")
numbers = "9,223,372,036,854,775,807"
print(numbers.split(sep=","))

# Usage of join and split together
print()
print("Usage of join and split together:")
# fmt: off
generate_list = [
    "9", " ",
    "2", "2", "3", " ",
    "3", "7", "2", " ",
    "0", "3", "6", " ",
    "8", "5", "4", " ",
    "7", "7", "5", " ",
    "8", "0", "7", " ",
]

# fmt: on
values = "".join(generate_list)
print(values)

values_list = values.split()
print(values_list)

# Mini Challenge
# Use a for loop to produce a list on ints, rather than strings.
# You can either modify the content of the 'values_list' list in place,
# or create a new list of ints.

print()
print("My answer for the mini challenge:")

print()
print("Approach b:")

int_values_list = []
for value in values_list:
    int_values_list.append(int(value))

print(int_values_list)

print()
print("Approach a:")

for index, value in enumerate(values_list):
    values_list[index] = int(value)

print(values_list)

# Actual answer is same
