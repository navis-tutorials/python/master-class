# Different ways to create lists

# With the help of square brackets, can be empty or can contain values
empty_list = []
even = [2, 4, 6, 8]
odd = [1, 3, 5, 7, 9]

# Create a new list by concatenating the old lists
numbers = even + odd
print(numbers)  # [2, 4, 6, 8, 1, 3, 5, 7, 9]

# Create a new list with the help of sorted function
sorted_numbers = sorted(numbers)
print(sorted_numbers)  # [1, 2, 3, 4, 5, 6, 7, 8, 9]
print(numbers)  # [2, 4, 6, 8, 1, 3, 5, 7, 9]

digits = sorted("432985617")
print(digits)  # ['1', '2', '3', '4', '5', '6', '7', '8', '9']

# Create a list with a list class by using iterables
digits = list("432985617")
print(digits)  # ['4', '3', '2', '9', '8', '5', '6', '1', '7']

more_numbers = list(numbers)
print(more_numbers)  # [2, 4, 6, 8, 1, 3, 5, 7, 9]
print(numbers is more_numbers)  # False
print(numbers == more_numbers)  # True

# Slice method to create a new list
sliced_numbers = numbers[:]
print(sliced_numbers)  # [2, 4, 6, 8, 1, 3, 5, 7, 9]

# Most effecient method is to use copy method
copied_numbers = numbers.copy()
print(copied_numbers)  # [2, 4, 6, 8, 1, 3, 5, 7, 9]
