current_choice = "-"  # Default choice
available_parts = [
    "computer",
    "monitor",
    "keyboard",
    "mouse",
    "mouse mat",
    "hdmi cable",
    "dvd drive",
    "speakers",
    "headphones",
]  # Create a default list for computer parts
computer_parts = []  # Create an empty list for computer parts

valid_choices = [str(i) for i in range(len(available_parts) + 1)]

while current_choice != "0":
    if current_choice in valid_choices:
        index = int(current_choice) - 1
        chosen_part = available_parts[index]

        if chosen_part in computer_parts:
            print("Removing {}".format(current_choice))
            computer_parts.remove(chosen_part)
        else:
            print("Adding {}".format(current_choice))
            computer_parts.append(chosen_part)

        print("Your list now contains: {}".format(computer_parts))
    else:
        print("Please choose an option from the list below:")
        for number, part in enumerate(available_parts):
            print("{}: {}".format(number + 1, part))

    current_choice = input()

print(computer_parts)

# Mini Challenge
# Change the program to have an another option, option 6 for an hdmi cable.
# Remeber that you will need to make a change in 3 places.
print()
print("My answer for the Mini Challenge:")

current_choice = "-"  # Default choice
computer_parts = []  # Create an empty list for computer parts

while current_choice != "0":
    if current_choice in "123456":
        print("Adding {}".format(current_choice))

        if current_choice == "1":
            computer_parts.append("computer")
        elif current_choice == "2":
            computer_parts.append("monitor")
        elif current_choice == "3":
            computer_parts.append("keyboard")
        elif current_choice == "4":
            computer_parts.append("mouse")
        elif current_choice == "5":
            computer_parts.append("mouse mat")
        elif current_choice == "6":
            computer_parts.append("hdmi cable")
    else:
        print("Please choose an option from the list below:")
        print("1: computer")
        print("2: monitor")
        print("3: keyboard")
        print("4: mouse")
        print("5: mouse mat")
        print("6: hdmi cable")
        print("0: Exit")

    current_choice = input()

print(computer_parts)

# Actual answer is same as above
