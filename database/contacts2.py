import sqlite3

db = sqlite3.connect("contacts.sqlite")

new_email = "navi@update.com"
phone = input("Please input the number: ")

# update_sql = f"UPDATE contacts SET email='{new_email}' WHERE phone={phone}"

# Sanitizing the input
update_sql = f"UPDATE contacts SET email=? WHERE phone=?"
update_cursor = db.cursor()
update_cursor.execute(update_sql, (new_email, phone))
print(f"{update_cursor.rowcount} rows affected.")

print()
print(f"Are connections the same: {update_cursor.connection == db}")

update_cursor.connection.commit()

update_cursor.close()

print()
for name, phone, email in db.execute("SELECT * FROM contacts"):
    print(name)
    print(phone)
    print(email)
    print("-" * 20)

db.close()
