import sqlite3

db = sqlite3.connect("contacts.sqlite")

db.execute("CREATE TABLE IF NOT EXISTS contacts (name TEXT, phone INTEGER, email TEXT)")
db.execute("INSERT INTO contacts(name, phone, email) VALUES('Navi', 123456, 'navi@mail.com')")
db.execute("INSERT INTO contacts VALUES('Navi Singh', 1234567890, 'navisingh@mail.com')")

cursor = db.cursor()
cursor.execute("SELECT * FROM contacts")

# Code to fetch every row
# for row in cursor:
#     print(row)

# Code to fetch and extract tuple elements
# for name, phone, email in cursor:
#     print(name)
#     print(phone)
#     print(email)
#     print("-" * 20)

# Code to fetch all records in the list consist of tuples
# print(cursor.fetchall())

# Code to fetch all records in the list consist of tuples
print(cursor.fetchone())
print(cursor.fetchone())
print(cursor.fetchone())  # It will show None because there are only two records

cursor.close()
db.commit()
db.close()
