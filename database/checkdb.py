import sqlite3

db = sqlite3.connect("contacts.sqlite")

name = input("Please enter a name to search for: ")

cursor = db.cursor()
cursor.execute("SELECT * FROM contacts WHERE name LIKE ?", (name,))

for row in cursor:
    print(row)

cursor.close()
db.close()
