a_string = 'this is\na string split\t\tand tabbed'
print(a_string)

raw_string = r"this is\na string split\t\tand tabbed"
print()
print(raw_string)

b_string = "this is" + chr(10) + "a string split" + chr(9) + chr(9) + "and tabbed"
print()
print(b_string)
