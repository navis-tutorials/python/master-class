class Tag(object):

    def __init__(self, name, contents):
        self.start_tag = f"<{name}>"
        self.end_tag = f"</{name}>"
        self.contents = contents

    def __str__(self):
        return f"{self.start_tag}{self.contents}{self.end_tag}"

    def display(self, file=None):
        print(self, file=file)


class DocType(Tag):

    def __init__(self):
        super().__init__('!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" '
                         '"http://www.w3.org/TR/html4/strict.dtd"', "")
        self.end_tag = ""  # DOCTYPE doesn't have an end tag


class Head(Tag):

    def __init__(self):
        super(Head, self).__init__('head', '')
        self.head_contents = []

    def add_tag(self, name, contents):
        new_tag = Tag(name=name, contents=contents)
        self.head_contents.append(new_tag)

    def display(self, file=None):
        for item in self.head_contents:
            self.contents += str(item)

        super(Head, self).display(file=file)


class Body(Tag):

    def __init__(self):
        super(Body, self).__init__('body', '')  # body contents will be built up separately
        self.body_contents = []

    def add_tag(self, name, contents):
        new_tag = Tag(name, contents)
        self.body_contents.append(new_tag)

    def display(self, file=None):
        for item in self.body_contents:
            self.contents += str(item)

        super().display(file=file)


class HtmlDoc(object):

    def __init__(self, doc_type, head, body):
        self._doc_type = doc_type
        self._head = head
        self._body = body

    def add_head_tag(self, name, contents):
        self._head.add_tag(name, contents)

    def add_tag(self, name, contents):
        self._body.add_tag(name, contents)

    def display(self, file=None):
        self._doc_type.display(file=file)
        print("<html>", file=file)
        self._head.display(file=file)
        self._body.display(file=file)
        print("</html>", file=file)


if __name__ == "__main__":
    # my_page = HtmlDoc()
    # my_page.add_head_tag('title', 'Document Title')
    # my_page.add_tag('h1', 'Main Heading')
    # my_page.add_tag('h2', 'Sub Heading')
    # my_page.add_tag('p', 'This is paragraph that will appear on the page')
    # with open('./test.html', 'w') as test_html:
    #     my_page.display(file=test_html)

    new_doc_type = DocType()

    new_head = Head()
    new_head.add_tag('title', 'Aggregation document')

    new_body = Body()
    new_body.add_tag('h1', 'Aggregation')
    new_body.add_tag('p', 'Unlike <strong>composition</strong>, aggregation uses existing '
                          'instances of objects to build up another object.')
    new_body.add_tag('p', "The composed object doesn't actually own the objects that "
                          "it's composed of - if it's destroyed, those objects continue "
                          "to exist.")

    my_page = HtmlDoc(new_doc_type, new_head, new_body)

    with open('test2.html', 'w') as test_html:
        my_page.display(file=test_html)
