class Kettle(object):
    power_source = "electricity"

    def __init__(self, make, price) -> None:
        self.make = make
        self.price = price
        self.on = False

    def switch_on(self):
        self.on = True


# Create a new instance of the Class
kenwood = Kettle("Kenwood", 8.99)
print(kenwood.make)
print(kenwood.price)

# Update the price
print()
kenwood.price = 12.75
print(kenwood.price)

# Create a new instance of the Class
print()
hamilton = Kettle("Hamilton", 14.55)
print(f"Models: {kenwood.make}:{kenwood.price}, {hamilton.make}:{hamilton.price}")

# Another way to print the same output
print()
print("Models: {0.make}:{0.price}, {1.make}:{1.price}".format(kenwood, hamilton))

# Swithcing on the Kettle
print()
print(hamilton.on)
hamilton.switch_on()
print(hamilton.on)

# Another way to do the same as above
print()
print(kenwood.on)
Kettle.switch_on(kenwood)
print(kenwood.on)

# Add new attribute to kenwood instance
print()
kenwood.power = 1.5
print(kenwood.power)

# Get power sources
print()
Kettle.power_source = "atomic"
print(Kettle.power_source)
kenwood.power_source = "gas"
print(kenwood.power_source)
print(hamilton.power_source)

# Get info about the vars
print()
print(Kettle.__dict__)
print(kenwood.__dict__)
print(hamilton.__dict__)
