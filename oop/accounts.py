import datetime
import pytz


class Account:
    """Simple account class with balance"""

    @staticmethod
    def _current_time():
        utc_time = datetime.datetime.utcnow()
        return pytz.utc.localize(utc_time)

    def __init__(self, name, balance) -> None:
        # Declare private attributes with double underscores
        self.__name = name
        self.__balance = balance
        self.__transaction_list = [(Account._current_time(), balance)]
        print(f"Account created for {self.__name}")

    def deposit(self, amount):
        if amount > 0:
            self.__balance += amount
            self.__transaction_list.append(
                (Account._current_time(), amount)
            )
            self.show_balance()

    def withdraw(self, amount):
        if 0 < amount <= self.__balance:
            self.__balance -= amount
            self.__transaction_list.append((Account._current_time(), -amount))
        else:
            print(
                "The amount must be greater than zero and no more than your account balance"
            )

        self.show_balance()

    def show_balance(self):
        print(f"Current balance is {self.__balance}")

    def show_transactions(self):
        for date, amount in self.__transaction_list:
            if amount > 0:
                trans_type = "deposited"
            else:
                trans_type = "withdrawn"
                amount *= -1

            print(
                f"{amount:6} {trans_type} on {date} (local time was {date.astimezone()})"
            )


if __name__ == "__main__":
    navi = Account("Navi Singh", 0)

    navi.show_balance()

    navi.deposit(500)
    # account.show_balance()
    navi.withdraw(140)
    # account.show_balance()
    navi.withdraw(1000)

    navi.show_transactions()

    # Add another account
    navneet = Account('Navneet', 800)
    navneet.deposit(100)
    navneet.withdraw(200)
    navneet.show_transactions()

    # Trying to update balance manually but will not work
    navneet.__balance = 50
    navneet.show_balance()
    print(navneet.__dict__)
