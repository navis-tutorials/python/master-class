import random


class Enemy(object):

    def __init__(self, name="Enemy", hit_points=0, lives=1):
        self._name = name
        self._hit_points = hit_points
        self._lives = lives
        self._alive = True

    @property
    def alive(self):
        return self._alive

    def take_damage(self, damage):
        remaining_damage = self._hit_points - damage
        if remaining_damage >= 0:
            self._hit_points = remaining_damage
            print(f"I took {damage} points damage and have {self._hit_points} left")
        else:
            self._lives -= 1
            if self._lives > 0:
                print(f"{self._name} lost a life")
            else:
                print(f"{self._name} is dead")
                self._alive = False

    def __str__(self):
        return f"Name: {self._name}, Hit Points: {self._hit_points}, Lives: {self._lives}"


class Troll(Enemy):

    def __init__(self, name):
        # super(Troll, self).__init__(name=name, hit_points=25, lives=1)
        # Another way for above
        super().__init__(name=name, hit_points=25, lives=1)

    def grunt(self):
        print(f"Me {self._name}. {self._name} stomp you")


class Vampyre(Enemy):

    def __init__(self, name):
        super().__init__(name=name, hit_points=12, lives=3)

    def dodges(self):
        if random.randint(1, 3) == 3:
            print(f"***** {self._name} dodges *****")
            return True
        else:
            return False

    def take_damage(self, damage):
        if not self.dodges():
            super().take_damage(damage=damage)


class VampyreKing(Vampyre):

    def __init__(self, name):
        super(VampyreKing, self).__init__(name)
        self._hit_points = 140

    def take_damage(self, damage):
        super(VampyreKing, self).take_damage(damage // 4)
