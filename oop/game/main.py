from player import Player
from enemy import Enemy, Troll, Vampyre, VampyreKing

# Create a new player
print("***** Player Navi *****")

navi = Player('Navi')

# Create a new Enemy
print()
print("***** Random Monster *****")

random_monster = Enemy("Basic Enemy", 12, 1)
print(random_monster)

random_monster.take_damage(4)
print(random_monster)

random_monster.take_damage(8)
print(random_monster)

random_monster.take_damage(9)
print(random_monster)

# Create a new Troll
print()
print("***** Ugly Troll *****")

ugly_troll = Troll("Pug")
print(f"Ugly Troll - {ugly_troll}")

# Create a new Troll
print()
print("***** Ug Troll *****")

ug_troll = Troll('Ug')
print(f"Ug Troll - {ug_troll}")

ug_troll.take_damage(15)
print(ug_troll)

ug_troll.take_damage(8)
print(ug_troll)

ug_troll.take_damage(8)
print(ug_troll)

# Create a new Troll
print()
print("***** Urg Troll *****")

urg_troll = Troll('Urg')
print(f"Urg Troll - {urg_troll}")

# Call grunt method
print()
print("***** Grunt Method *****")

ugly_troll.grunt()
ug_troll.grunt()
urg_troll.grunt()

# Create a new Vampyre
print()
print("***** Bella Swan Vampyre *****")

bella_swan = Vampyre('Bella Swan')
print(bella_swan)

bella_swan.take_damage(4)
print(bella_swan)

bella_swan.take_damage(8)
print(bella_swan)

bella_swan.take_damage(8)
print(bella_swan)

# Create a new Vampyre
print()
print("***** Edward Cullen Vampyre *****")

edward_cullen = Vampyre('Edward Cullen')
print(edward_cullen)

while edward_cullen.alive:
    edward_cullen.take_damage(1)
    print(edward_cullen)

# Create a new Vampyre
print()
print("***** Blade Vampyre *****")

blade = VampyreKing('Blade')
print(blade)

while blade.alive:
    blade.take_damage(16)
    print(blade)
