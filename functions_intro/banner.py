def banner_text(text: str = " ", screen_width: int = 80) -> None:
    """
    Print banner on the standard output.

    Args:
        text (str, optional): Text to print on the stdout. Defaults to " ".
        screen_width (int, optional): Width of the screen. Defaults to 80.

    Raises:
        ValueError: Exception raised when the text is wider than the screen width.
    """
    border_size = 4

    if len(text) > screen_width - border_size:
        # print("EEK!!")
        # print("THE TEXT IS TOO LONG TO FIT IN THE SPECIFIED WIDTH")
        raise ValueError(
            "String '{}' is larger than the specified width {}".format(
                text, screen_width
            )
        )

    if text == "*":
        print("*" * screen_width)
    else:
        print("**{}**".format(text.center(screen_width - border_size)))


banner_text("*")
banner_text("Always look on the bright side of life...")
banner_text("If life seems jolly rotten,")
banner_text("There's something you've forgotten!")
banner_text("And that's to laugh and smile and dance and sing,")
banner_text(screen_width=60)
banner_text("When you're feeling in the dumps,")
banner_text("Don't be silly chumps,")
banner_text("Just purse your lips and whistle - that's the thing!")
banner_text("And... always look on the bright side of life...")
banner_text("*")

# Banner Text Challenge

# Modify the banner_text function so that it takes another argument, the width for the banner.


# Acutall answer is same as above
