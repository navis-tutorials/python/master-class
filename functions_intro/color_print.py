"""Some ANSI escape sequences for colors and effects"""
from typing import Reversible


BLACK = "\u001b[30m"
RED = "\u001b[31m"
GREEN = "\u001b[32m"
YELLOW = "\u001b[33m"
BLUE = "\u001b[34m"
MAGENTA = "\u001b[35m"
CYAN = "\u001b[36m"
WHITE = "\u001b[37m"
RESET = "\u001b[0m"

BOLD = "\u001b[1m"
UNDERLINE = "\u001b[4m"
REVERSE = "\u001b[7m"

print(RED, "this will be in red")
print("and so is this")


def color_print(text: str, *effects: str) -> None:
    """
    Print `text` using ANSI sequences to change color, etc.

    Args:
        text (str): The text to print.
        effects (str): The effects we want.
    """
    effect_string = "".join(effects)
    output_string = "{0}{1}{2}".format(effect_string, text, RESET)
    print(output_string)


color_print("Hello, Green", GREEN)
color_print("Hello, Red", RED)
color_print("Hello, Red and Bold", RED, BOLD)
print("This should be in the default terminal color.")
color_print("Hello, Blue, Bold, and Reverse", BLUE, BOLD, REVERSE)
color_print("Hello, Bold", BOLD)
color_print("Hello, Underline", UNDERLINE)
color_print("Hello, Yellow and Underline", YELLOW, UNDERLINE)
color_print("Hello, Reverse", REVERSE)
