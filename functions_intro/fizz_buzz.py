"""Fizz Buzz Game"""

FIZZ = "Fizz"
BUZZ = "Buzz"
FIZZ_BUZZ = "Fizz Buzz"

number = 1
turn_computer = True


def fizz_buzz(num: int) -> str:
    """
    Calculate correct answer

    Args:
        number (int): Number to check

    Returns:
        str: Return string number, or Fizz, or Buzz, or Fizz Buzz
    """
    answer = str(num)

    if num % 15 == 0:
        answer = FIZZ_BUZZ
    elif num % 5 == 0:
        answer = BUZZ
    elif num % 3 == 0:
        answer = FIZZ

    return answer


while number < 101:
    actual_answer = fizz_buzz(number)

    if not turn_computer:
        user_answer = input("Count next number: ")

        if actual_answer.casefold() == user_answer.casefold():
            print(f"You ({number}): Correct answer")
        else:
            print(f"You ({number}): Wrong answer")
            break
    else:
        print(f"Computer ({number}): {actual_answer}")

    number += 1
    turn_computer = not turn_computer

else:
    print("Congratulations you won!")


# Actual answer is same as above
