# Define a custom function
def multiply():
    """
    Multipy `float` value with `int` value.

    Returns:
        float: Returns the result of multiplication.
    """
    result = 10.5 * 4
    return result


# Function call without parameter
print("Function call without parameter:")
answer = multiply()
print(answer)

# Define a custom function with two parameters
def multiply_v2(x, y):
    """
    Mulitply two values

    Args:
        x (numeric): The first parameter
        y (numeric): The second parameter

    Returns:
        numeric: Result of the multiplication
    """
    result = x * y
    return result


# Function call with two arguments
print()
print("Function call with two arguments:")
answer = multiply_v2(10.0, 5)  # Passing two arguments to the function
print(answer)

# Function call to multiply the value two times
print()
print("Function call to multiply the value two times:")
for val in range(1, 5):
    two_times = multiply_v2(2, val)
    print(two_times)


# Palindrome
def is_palindrome(string):
    """
    Check if the provided string is palindrome or not.

    Args:
        string (str): The string to check

    Returns:
        bool: Return True of False
    """
    return string[::-1].casefold() == string.casefold()


print()
word = input("Please enter a word to check: ")
if is_palindrome(word):
    print("{} is a palindrome".format(word))
else:
    print("{} is not a palindrome.".format(word))


# Mini Challenge
# Fix our is_palindrome function so that it ignores the case of the letters, when checking if the two strings are equal.
# Remember to test the function with words that are palindromes, using a mix of upper and lower case letters.
# Also test that it correctly identifies words that aren't palindromes.


# Actual answer is same as above


# Palindrome Sentence Challenge

# Create a new function called palindrome_sentence.
# The function should return true if the sentence is a palindrome - False, otherwise.
# Remember that we ignore spaces, punctuation and things like tabs and line feeds. We're only interested in alphanumeric characters.
# Check out the string methods in the documentation, to find one that's suitable for identifying if a character is alphanumeric.
# There are two methods you could use, depending on whether you want to allow numbers or not.

print()
print("Palindrome sentence challenge answer:")


def palindrome_sentence(string):
    """
    Sanitize the provided string sentence and then check if the provided string is palindrome or not.

    Args:
        string (str): The string sentence

    Returns:
        bool: Return True or False
    """
    sanitized_string = ""

    for char in string:
        if char.isalnum():
            sanitized_string += char

    return is_palindrome(sanitized_string)


# sentence = "Desnes not far, Rafton sensed."
# sentence = "Do geese see god?"
# sentence = "Was it a car, or a cat, I saw?"

sentence = input()
if palindrome_sentence(sentence):
    print("'{}' is a palindrome.".format(sentence))
else:
    print("'{}' is not a palindrome.".format(sentence))


# Actual answer is same as above


def fibonacci(n):
    """
    Return the `n`th fibonacci number, for positive `n`.

    Args:
        n (int): number to generate fibonacci series

    Returns:
        int: `n`th fibonacci number
    """

    if 0 <= n <= 1:
        return n

    n_minus1, n_minus2 = 1, 0

    result = None
    for f in range(n - 1):
        result = n_minus2 + n_minus1
        n_minus2 = n_minus1
        n_minus1 = result

    return result


for i in range(16):
    print(i, fibonacci(i))
