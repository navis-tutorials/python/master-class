"""Star arguments and parameter examples"""
numbers = (0, 1, 2, 3, 4, 5)

print(numbers)  # It will print the tuple as it is
print(*numbers)  # It will unpack the tuple and then print
print(0, 1, 2, 3, 4, 5)  # It is same thing as above


def test_star(*params):
    print(params)

    for x in params:
        print(x)


print()
test_star(0, 1, 2, 3, 4, 5)

print()
test_star()
