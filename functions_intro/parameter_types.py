"""Parameter Kinds"""


def func(p_1, p_2, *params, konly, **kwargs):
    """
    Different kinds of parameters

    Args:
        p1 (Any): Positional or Keyword
        p2 (Any): Positional or Keyword
        konly (Any): Keyword Only
    """
    print(f"positional-or-keyword:...{p_1}, {p_2}.")
    print(f"var-positional:..........{params}")
    print(f"keyword-only:............{konly}")
    print(f"var-keyword:.............{kwargs}")


func(1, 2, 3, 4, 5, 6, 7, konly=67, key1=20, key2=30)

# End of Section
