"""Import Modules"""
from turtle import forward, right, done  # Specific imports from the module

from turtle import *  # Another way to import everything from the module

import turtle  # Import everything from the module
import time

turtle.forward(150)
turtle.right(250)
turtle.forward(150)

# forward(150) # Use them as is it is
# right(250) # Use them as is it is
# forward(150) # Use them as is it is

turtle.done()  # Use this if you don't want to use time.sleep() method
time.sleep(5)  # Put program on sleep for 5 seconds
