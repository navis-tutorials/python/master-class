"""Parabola Example"""
try:
    import tkinter
except ImportError:
    import Tkinter as tkinter


def parabola(value):
    """Calculate Parabola

    Args:
        value ([type]): [description]

    Returns:
        [type]: [description]
    """
    return value * value / 100


def draw_axes(canva: tkinter.Canvas):
    """Draw Axis

    Args:
        canva (tkinter.Canvas): [description]
    """
    canva.update()

    x_origin = canva.winfo_width() / 2
    y_origin = canva.winfo_height() / 2

    canva.configure(scrollregion=(-x_origin, -y_origin, x_origin, y_origin))
    canva.create_line(-x_origin, 0, x_origin, 0, fill="black")
    canva.create_line(0, y_origin, 0, -y_origin, fill="black")

    # Print available local variables
    print(locals())


def plot(canva: tkinter.Canvas, x_orig: int, y_orig: int):
    """Plot the lines

    Args:
        canva (tkinter.Canvas): [description]
        x_orig (int): [description]
        y_orig (int): [description]
    """
    canva.create_line(x_orig, y_orig, x_orig + 1, y_orig + 1, fill="red")


# Create main window instance
mainWindow = tkinter.Tk()
mainWindow.title("Parabola")
mainWindow.geometry("740x480+300+150")

# Create canvas
canvas = tkinter.Canvas(mainWindow, width=370, height=480)
canvas.grid(row=0, column=0)

# Create another canvas
canvas2 = tkinter.Canvas(mainWindow, width=370, height=480, background="blue")
canvas2.grid(row=0, column=1)

# Draw axis by using function
print(repr(canvas), repr(canvas2))
draw_axes(canvas)
draw_axes(canvas2)

# Loop to print parabola from -100 to 100
for x in range(-100, 100):
    y = parabola(x)
    plot(canvas, x, -y)

# Start window
mainWindow.mainloop()
