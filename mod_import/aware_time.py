"""More on Timezones"""
import datetime
import pytz

local_time = datetime.datetime.now()
utc_time = datetime.datetime.utcnow()

aware_local_time = pytz.utc.localize(utc_time).astimezone()
aware_utc_time = pytz.utc.localize(utc_time)

print(f"Aware local time {aware_local_time}, time zone {aware_local_time.tzinfo}")
print(f"Aware UTC time {aware_utc_time}, time zone {aware_utc_time.tzinfo}")
