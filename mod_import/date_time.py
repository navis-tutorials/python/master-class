"""Timezones"""

import time

print(f"The epoch on this system starts at {time.strftime('%c', time.gmtime(0))}")

print(f"The current timezone is {time.tzname[0]} with an offset of {time.timezone}")

if time.daylight != 0:
    print("\tDaylight Time Saving is in effect for this location.")
    print(f"\tThe DST Timezone is {time.tzname[1]}")

print(f"Local time is {time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}")
print(f"UTC time is {time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())}")
