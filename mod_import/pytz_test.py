"""Using Timezones with PYTZ package"""
import datetime
import pytz

COUNTRY = "Europe/Moscow"

tz_to_display = pytz.timezone(COUNTRY)
local_time = datetime.datetime.now(tz=tz_to_display)

print(f"The time in {COUNTRY} is {local_time}")
print(f"UTC is {datetime.datetime.utcnow()}")

# Get all timezones
print()
print("Get all timezones:")

for timezone in pytz.all_timezones:
    print(timezone)

# Get all countries
print()
print("Get all countries:")

for country in sorted(pytz.country_names):
    print(f"{country}: {pytz.country_names[country]}")

# Get all countries along with timezones
print()
print("Get all countries along with timezones:")

for country in sorted(pytz.country_names):
    print(
        f"{country}: {pytz.country_names[country]}: {pytz.country_timezones.get(country)}"
    )
