"""Mini Challenge"""
# Write a small program to display information on the
# five clocks whose functions we have just looked at:
# i.e. time(), perf_counter(), monotonic(), thread_time() and process_time()
#
# Use the documenatation for the get_clock_info() function
# to work out how to call for each of the clocks.

from time import get_clock_info

clocks = ("monotonic", "perf_counter", "process_time", "thread_time", "time")

for clock in clocks:
    print(f"Clock name `{clock}`:\t {get_clock_info(clock)}")
