"""Standard Python Libraries"""
import shelve

# Print inbuilt libraries, functions, methods
print("Print inbuilt libraries, functions, methods:")
print(dir())

# It will print available libraries, functions, methods for __builtins__
print()
print("Available libraries, functions, methods for __builtins__:")
for m in dir(__builtins__):
    print(m)

print()
print("Available functions, methods for shelve module:")
# It will print the available functions, methods for shelve module
print(dir(shelve))

# It will print the available functions, methods for Shelf module
print()
print("Available functions, methods for Shelf module:")
for obj in dir(shelve.Shelf):
    if obj[0] != "_":
        print(obj)


# Get help related to the topic, function, class, etc.
print()
print("Get help related to the topic, function, class, etc.:")
help(shelve)
