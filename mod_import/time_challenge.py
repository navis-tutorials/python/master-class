"""Time Challenge"""
# Create a program that allows a user to choose one of
# up to 9 time zones from a menu. You can choose any
# zones you want from the all_timezones list.
#
# The program will then display the time in that timezone, as
# well as local time and UTC time.
#
# Entering 0 as the choice will quit the program.
#
# Display the dates and times in a format suitable for the
# user of your program to understand, and include the
# timezone name when displaying the chosen time.

import datetime
import pytz

time_zones = (
    "America/New_York",
    "Asia/Bangkok",
    "Europe/Stockholm",
    "Asia/Riyadh",
    "Europe/Moscow",
    "Pacific/Auckland",
    "Europe/Amsterdam",
    "Asia/Kuala_Lumpur",
    "Asia/Kolkata",
)


def draw_menu() -> None:
    """
    Function to draw a choice menu for timezones
    """
    print("0: Quit")

    for index, (time_zone) in enumerate(sorted(time_zones)):
        print(f"{index + 1}: {time_zone}")


draw_menu()

while True:
    choice = int(input("Please enter your choice:"))

    if choice == 0:
        print("Thank You for playing!")
        break

    if choice in range(1, 10):
        selected_zone = pytz.timezone(time_zones[choice - 1])

        local_time = datetime.datetime.now()
        aware_local_time = pytz.utc.localize(local_time).astimezone()

        print(
            f"Selected timezone: {time_zones[choice -1]}, time is {datetime.datetime.now(tz=selected_zone)}"
        )
        print(f"Local timezone: {aware_local_time.tzinfo}, time is {local_time}")
        print(f"UTC time is {datetime.datetime.utcnow()}")
    else:
        print("Please enter a valid choice.")
        draw_menu()
