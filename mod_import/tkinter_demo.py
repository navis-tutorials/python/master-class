"""Tkinter Demo"""
try:
    import tkinter
except ImportError:  # python 2
    import Tkinter as tkinter

print(tkinter.TkVersion)
print(tkinter.TclVersion)

# Testing GUI window
# tkinter._test()

# Create custom window
mainWindow = tkinter.Tk()

# mainWindow.geometry('640x480') # Mentioned width and height
mainWindow.title("Navi Singh")

# Mentioned dimensions along with position offset
# mainWindow.geometry("640x480+400+200")
# Use negative position offset
mainWindow.geometry("640x480-400-200")

# Add label to the main window
label = tkinter.Label(mainWindow, text="Hello World")
label.pack(side="top")
# Use Grid instead of Pack
label.grid(row=0, column=0)

# Add Frame on the left hand side
leftFrame = tkinter.Frame(mainWindow)
# leftFrame.pack(side="left", fill=tkinter.Y, expand=False, anchor="n")
# Use Grid instead of Pack
leftFrame.grid(row=1, column=1)

# Add Canvas to the main window
canvas = tkinter.Canvas(leftFrame, relief="raised", borderwidth=1)
# canvas.pack(side="left", anchor="n")
# We can also use fill and expand properties for the canvas
# canvas.pack(side="top",fill=tkinter.X)
# canvas.pack(side="top", fill=tkinter.Y, expand=True)
# Use Grid instead of Pack
canvas.grid(row=1, column=0)

# Add Frame on the right hand side
rightFrame = tkinter.Frame(mainWindow)
# rightFrame.pack(side="right", expand=True, anchor="n")
# Use Grid instead of Pack
rightFrame.grid(row=1, column=2, sticky="n")

# Add three buttons
button1 = tkinter.Button(rightFrame, text="Button 1")
button2 = tkinter.Button(rightFrame, text="Button 2")
button3 = tkinter.Button(rightFrame, text="Button 3")

# Show buttons
# button1.pack(side="top")
# button2.pack(side="top")
# button3.pack(side="top")
# Use Grid instead of Pack
button1.grid(row=0, column=0)
button2.grid(row=1, column=0)
button3.grid(row=2, column=0)

# Configure the columns
mainWindow.columnconfigure(0, weight=1)
mainWindow.columnconfigure(1, weight=1)
mainWindow.grid_columnconfigure(2, weight=1)

mainWindow.mainloop()
