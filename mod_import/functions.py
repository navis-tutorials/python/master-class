def python_food():
    width = 80
    text = "Spam and Eggs"
    left_margin = width - len(text) // 2
    print(" " * left_margin, text)


def center_text_old(text):
    left_margin = 80 - len(str(text)) // 2
    print(" " * left_margin, text)


def center_text(*args, sep=" ", end="\n", file=None, flush=False):
    text = ""

    for arg in args:
        text += str(arg) + sep

    left_margin = 40 - len(text) // 2
    print(" " * left_margin, text, end=end, file=file, flush=flush)


# Call functions
python_food()

# Print and center the text
print()
print("Print and center the text")
center_text_old("spam and eggs")
center_text_old("spam, spam and eggs")
center_text_old(12)
center_text_old("spam, spam, spam and eggs")

# Normal Print function
print()
print("Normal Print function:")
print("first", "second", 3, 4, "spam")

# Print and center the text with the new function
print()
print("Print and center the text with the new function")
center_text("spam and eggs")
center_text("spam, spam and eggs")
center_text(12)
center_text("spam, spam, spam and eggs")
center_text("first", "second", 3, 4, "spam")

with open("./mod_import/centred", "w") as centred_file:
    center_text("spam and eggs", file=centred_file)
    center_text("spam, spam and eggs", file=centred_file)
    center_text(12, file=centred_file)
    center_text("spam, spam, spam and eggs", file=centred_file)
    center_text("first", "second", 3, 4, "spam", file=centred_file)
