"""Mini Challenge Module"""
# Create a program that takes some text and returns a list
# of all characters in the text that are not vowels, sorted in
# alphabetical order.
#
# You can either enter the text from the keyboard or
# initialise a string variable with the string.

string = "Python is a very powerful language"
# vowels = {"a", "e", "i", "o", "u"}
vowels = frozenset("aeiou")

# final_list = []
# for char in string:
#     if char not in vowels:
#         final_list.append(char)

# print(sorted(final_list))

final_set = set(string).difference(vowels)
print(final_set)
print(sorted(final_set))
