"""Introduction to Sets
1. Sets are unordered and just like a dictionary
2. Similar to Mathematical Sets
3. Same way to decalare as dictionary but without keys
"""

# Basic way to declare a set
print("Basic way to declare a set:")
farm_animals = {"sheep", "cow", "hen"}
print(farm_animals)

for animal in farm_animals:
    print(animal)

# Another way to deaclare a set
print()
print("Another way to deaclare a set:")
wild_animals = set(["lion", "tiger", "panther", "elephant", "hare"])
print(wild_animals)

for animal in wild_animals:
    print(animal)

# Add more element in the set
print()
print("Add more element in the set:")
farm_animals.add("horse")
wild_animals.add("horse")
print(farm_animals)
print(wild_animals)

# Declare an empty set
print()
print("Declare an empty set:")
empty_set = {}  # It will declare an empty dictionary not set
emprty_set_2 = set()
print(type(empty_set))
print(type(emprty_set_2))

# Use any type of sequence to add elements in the set
print()
print("Use any type of sequence to add elements in the set:")
even = range(0, 40, 2)  # Used range sequence type
even_set = set(even)
print(even_set)

squares_tuple = (4, 6, 9, 16, 25)  # Used tuple squence type
squares = set(squares_tuple)
print(squares)

# union() method to combine two sets
print()
print("union() method to combine two sets:")
even_union = even_set.union(squares)
print(even_set)
print(len(even_set))
print(squares)
print(len(squares))
print(even_union)
print(len(even_union))

# Use intersection() method or & operator to get the intersection of two sets
print()
print("Use intersection() method or & operator to get the intersection of two sets:")
print(even_set.intersection(squares))
print(even_set & squares)
print(squares.intersection(even_set))
print(squares & even_set)

# Use difference() method or - operator to remove the same elements from set A comparing with set B
print()
print(
    "Use difference() method or - operator to remove the same elements from set A comparing with set B:"
)

print(sorted(even_set.difference(squares)))
print(sorted(even_set - squares))
print(sorted(squares.difference(even_set)))
print(sorted(squares - even_set))

# Use difference_update() method to calculate the difference and update the same set
print()
print(
    "Use difference_update() method to calculate the difference and update the same set:"
)
even_set_copy = even_set.copy()
print(even_set_copy)
even_set_copy.difference_update(squares)
print(even_set_copy)

# Use symmetric_difference() method to remove common elements from the two sets
print()
print("Use symmetric_difference() method to remove common elements from the two sets:")
print(even_set)
print(squares)
print(even_set.symmetric_difference(squares))
print(even_set ^ squares)

# Use discard() or remove() method to remove an element from the set
# discard() will not raise an error if element doesn't exists
# remove() will raise an error if element doesn't exists
print()
print("Use discard() or remove() method to remove an element from the set:")
print(even_set_copy)
even_set_copy.discard(14)
even_set_copy.remove(20)
even_set_copy.discard(14)

try:
    even_set_copy.remove(14)
except KeyError:
    print("The item 14 is not a member of the set")

print(even_set_copy)

# Use issubset() to check if the set is subset
# Use issuperset() to check if the set is superset
print()
print("check issubset() and issuperset():")

squares.discard(9)
squares.discard(25)
print(squares)
print(even_set)
print(squares.issubset(even_set))
print(squares <= even_set)
print(even_set.issuperset(squares))
print(even_set >= squares)
