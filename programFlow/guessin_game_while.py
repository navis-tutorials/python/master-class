"""
Simple make a guess game

Returns:
    int: Actual number that is thought by the user.
"""
import random

# get_integer challenge
# Modify the get_integer function so that it prints a message, if the user enters an invalid number.


def get_integer(prompt):
    """
    Get an integer from Standard Input (stdin).

    The function will continue to looping, and prompting the user, until a valid `int` is entered.

    Args:
        prompt (str): The String that the user will see, when they're prompted to enter the value

    Returns:
        int: The integer that the user enters.
    """
    while True:
        temp = input(prompt)
        if temp.isnumeric():
            return int(temp)
        # else:
        print("Please enter a valid number.")


# Actual answer

highest = 10
answer = random.randint(1, highest)
print(answer)  # TODO Remove after testing

# Get the user input
print("Please guess a number between 1 and {}: ".format(highest))
guess = get_integer(": ")

# Add conditions to show the appropriate message
if guess < answer:
    print("Please guess higher.")

    guess = int(input())
    if guess == answer:
        print("Well done, you guessed it!")
    else:
        print("Sorry, you have not guessed correctly")
elif guess > answer:
    print("Please guess lower.")

    guess = int(input())
    if guess == answer:
        print("Well done, you guessed it!")
    else:
        print("Sorry, you have not guessed correctly")
else:
    print("You got it first time.")

# Challenge
# Modify the program to use a while loop, to allow the user to keep guessing.
# The program should let the player know whether to guess higher or lower, and should print a messsage when the guess is correct.
# As an optional extra, allow the user to quit by entering 0(zero) for their guess.

print()
print("My answer:")

# Get the user input
print()
print("Please guess a number between 1 and {}: ".format(highest))
guess = int(input())

if guess == answer:
    print("You got it first time.")
else:
    while True:
        if guess == 0:
            print("Game over.")
            break
        elif guess > answer:
            print("Please guess lower.")
            guess = int(input())
        elif guess < answer:
            print("Please guess higher.")
            guess = int(input())
        else:
            print("Well done, you guessed it!")
            break

# Actual answer
print()
print("Actual answer:")

print()
print("Please guess a number between 1 and {}: ".format(highest))

guess = 0

while guess != answer:
    guess = int(input())

    if guess == 0:
        print("Game Over.")
        break

    if guess == answer:
        print("Well done, you guessed it.")
        break

    if guess > answer:
        print("Please guess lower")
    else:
        print("Please guess higher.")
