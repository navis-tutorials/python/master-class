for i in range(1, 11):
    for j in range(1, 11):
        print("{:2} times {:2} is {:3}".format(j, i, i * j))
    print("-" * 18)
