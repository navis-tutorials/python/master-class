# Simple make a guess game
answer = 5

# Get the user input
print("Please guess a number between 1 and 10: ")
guess = int(input())

# Add conditions to show the appropriate message
if guess < answer:
    print("Please guess higher.")

    guess = int(input())
    if guess == answer:
        print("Well done, you guessed it!")
    else:
        print("Sorry, you have not guessed correctly")
elif guess > answer:
    print("Please guess lower.")

    guess = int(input())
    if guess == answer:
        print("Well done, you guessed it!")
    else:
        print("Sorry, you have not guessed correctly")
else:
    print("You got it first time.")

# Another short way to achieve the same result as above without redundant code.
print()
print("Another way to achieve the same result: ")

# Get the user input
print("Please guess a number between 1 and 10: ")
guess = int(input())

if guess != answer:
    if guess < answer:
        print("Please guess higher.")
    else:
        print("Please guess lower")

    guess = int(input())
    if guess == answer:
        print("Well done, you guessed it!")
    else:
        print("Sorry, you have not guessed correctly.")
else:
    print("You got it first time.")

# Challenge
# Change the condition on the line 32 to
# if guess == answer:
# then change the program to give the correct results.

# My Answer
print()
print("My answer:")

# Get the user input
print("Please guess a number between 1 and 10: ")
guess = int(input())

if guess == answer:
    print("You got it first time.")
else:
    if guess < answer:
        print("Please guess higher.")
    else:
        print("Please guess lower")

    guess = int(input())
    if guess == answer:
        print("Well done, you guessed it!")
    else:
        print("Sorry, you have not guessed correctly.")

# Actual answer was same as mine.
