parrot = "Norwegian Blue"

# Basic for loop demonstration by using string sequence
for character in parrot:
    print(character)

# Get separators from a string by using for loop
print()
print("Get separators from a string by using for loop:")

number = "9,223;372:036 854,775;807"
separators = ""

for char in number:
    if not char.isnumeric():
        separators = separators + char

print(separators)

values = "".join(char if char not in separators else " " for char in number).split()
print([int(val) for val in values])

# Making the above program more interactive
print()
print("Making the above program more interactive:")

number = input("Please enter a series of numbers, using any separators you like: ")
separators = ""

for char in number:
    if not char.isnumeric():
        separators = separators + char

print(separators)

values = "".join(char if char not in separators else " " for char in number).split()

# It will print the sum of the values sequence
print(sum([int(val) for val in values]))
