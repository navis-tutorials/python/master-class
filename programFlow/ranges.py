# Range is a sequence or iterable object
print("Range is a sequence or iterable object:")

for i in range(10, 16):
    print("i is now {}".format(i))

# Start is optional if step is not mentioned
print()
print("Start is optional if step is not mentioned:")
for i in range(5):
    print("i is now {}".format(i))

# Start is required if step is mentioned
print()
print("Start is required if step is mentioned:")
for i in range(10, 16, 2):
    print("i is now {}".format(i))

# Negative steps
# * Start value should be greater than the end value
print()
print("Negative steps:")
for i in range(10, 4, -2):
    print("i is now {}".format(i))

# Usage with if statement
print()
print("Usage with if statement:")
age = int(input("Please enter your age: "))

if age in range(16, 66):
    print("Have a good day at work!")
else:
    print("Enjoy your free time.")
