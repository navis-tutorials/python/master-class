# Write a program to print a number of options, and allow the user to select an option from the list.
# The options should be numbered from 1 to 9 - although you can use less than 9 options if you want.
# Make sure there are at least 4 options.

# Example:
# Please choose your option from the list below:
# 1. Learn Python
# 2. Learn Java
# 3. Go swimming
# 4. Have dinner
# 5. Go to bed
# 0. Exit

# The program should continue looping, allowing the use to choose one of the options each time round.
# The loop should only terminate when the user chooses 0.
# If the user makes a valid choice, then print a short message.
# The message should include the value that they typed.

# As an optional extra, modify the program so that the menu is printed again, if they choose an invalid option.

print("My Answer to challenge:")

options = [
    "Exit",
    "Learn Python",
    "Learn Java",
    "Learn Go",
    "Learn NodeJS",
    "Learn React JS",
    "Go swimming",
    "Have dinner",
    "Go to bed",
]

options_message = "Please choose your option from the list below:"
for index in range(1, len(options)):
    options_message += "\n{}. {}".format(index, options[index])
options_message += "\n0. Exit"

print(options_message)

while True:
    selected_option = int(input())

    if selected_option in range(len(options)):
        print("You have selected: {}".format(options[selected_option]))

        if selected_option == 0:
            print("Good Bye!")
            break
    else:
        print(options_message)

print()
print("Actual answer:")

choice = "-"
while True:
    if choice == "0":
        print("You chose {}".format(choice))
        break
    elif choice in "12345":
        print("You chose {}".format(choice))
    else:
        print("Please choose your option from the list below:")
        print("1:\tLearn Python")
        print("2:\tLearn Java")
        print("3:\tGo Swimming")
        print("4:\tHave dinner")
        print("5:\tGo to bed")
        print("0:\tExit")

    choice = input()

print()
print("Another way to implement while:")

choice = "-"
while choice != "0":
    if choice in "12345":
        print("You chose {}".format(choice))
    else:
        print("Please choose your option from the list below:")
        print("1:\tLearn Python")
        print("2:\tLearn Java")
        print("3:\tGo Swimming")
        print("4:\tHave dinner")
        print("5:\tGo to bed")
        print("0:\tExit")

    choice = input()
