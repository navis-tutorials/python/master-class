# Simple while loop
print("Simple while loop:")

i = 0
while i < 5:
    print("i is now {}".format(i))
    i += 1
