# Predefined directions that the user can choose to exit the program
available_exits = ["north", "south", "east", "west"]
# Default chosen value
chosen_exit = ""

# Until the user choose the correct value
while chosen_exit not in available_exits:
    chosen_exit = input("Please choose a direction: ")
    if chosen_exit.casefold() == "quit":
        print("Game Over")
        break

else:
    # Complements
    print("Aren't you glad you get out of there")
