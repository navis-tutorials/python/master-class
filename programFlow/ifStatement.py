# Program to demonstrate If Statement
print("Program to demonstrate If Statement")

# Get the user input
name = input("Please enter your name: ")
age = int(input("How old are you, {}? ".format(name)))

# Print the age
print(age)

# Add if statement to check whether the user is eligible for voting or not
if age >= 18:
    print("You are old enough to vote")
    print("Please put an X in the box")
else:
    print("Please come back in {} years.".format(18 - age))

# Another way to get the same result
print()
print("Another way to get the same result:")
if age < 18:
    print("Please come back in {} years.".format(18 - age))
else:
    print("You are old enough to vote")
    print("Please put an X in the box")

# Usage of elif statement
print()
print("Usage of elif statement:")
if age < 18:
    print("Please come back in {} years.".format(18 - age))
elif age == 900:
    print("Sorry, Yada, you die in return of the Jedi")
else:
    print("You are old enough to vote")
    print("Please put an X in the box")
