# List is use store different type of data just like an array
print("Shopping List:")

shoppingList = ["milk", "pasta", "chaap", "spam", "bread", "rice"]

# Iterate through the list and use continue keyword
print()
print("continue keyword:")

for item in shoppingList:
    # Check if an item is spam then skip it
    if item == "spam":
        continue

    # Otherwise print the items in the shopping list
    print("Buy {}".format(item))

# Iterate through the list and use break keyword
print()
print("break keyword:")

item_to_found = "butter"
found_at = None

for index in range(len(shoppingList)):
    # Check if an item is spam then skip it
    if shoppingList[index] == item_to_found:
        found_at = index
        break

if found_at is not None:
    print("Item found at position {}".format(found_at))
else:
    print("{} not found".format(item_to_found))


# Another way to find an item from a list
print()
print("Another way to find an item from a list:")

item_to_found = "spam"
found_at = None

if item_to_found in shoppingList:
    found_at = shoppingList.index(item_to_found)

if found_at is not None:
    print("Item found at position {}".format(found_at))
else:
    print("{} not found".format(item_to_found))
