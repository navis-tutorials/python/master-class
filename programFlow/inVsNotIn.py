# Usage of in operator
print("Usage of in operator:")

parrot = "Norweigan Blue"

# Get user input
letter = input("Enter a character: ")

# Apply in operator to condition
if letter in parrot:
    print("{} letter found in {}.".format(letter, parrot))
else:
    print("I don't need this letter")

# Usage of not in operator
print()
print("Usage of not in operator:")

# Get user input
activity = input("What would you like to do today? ")

# Apply not in operator to condition
if "cinema" not in activity.casefold():
    print("But I want to go to the Cinema.")
else:
    print("Let's go!")
