# and Truth
# Truth and Truth = Truth
# False and Truth = False
# Truth and False = False
# False and False = False

# or Truth
# Truth and Truth = Truth
# False and Truth = Truth
# Truth and False = Truth
# False and False = False

# Usage of and operator
print("Usage of and operator:")

age = int(input("How old are you? "))
if age >= 16 and age <= 65:
    print("Have a good day at work.")
else:
    print("Enjoy your free time.")

# Simplify Chained Comparison
print()
print("Simply chained comparison of the above:")

if 16 <= age <= 65:
    print("Have a good day at work.")
else:
    print("Enjoy your free time.")

# Usage of or operator
print()
print("Usage of or operator:")

if age < 16 or age > 65:
    print("Enjoy your free time.")
else:
    print("Have a good day at work.")
