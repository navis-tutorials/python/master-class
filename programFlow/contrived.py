# else in a loop
print("else in a loop:")

numbers = [1, 34, 53, 37, 565]

for number in numbers:
    if number % 8 == 0:
        # reject the list
        print("The numbers are unacceptable")
        break
else:
    print("All those numbers are fine.")
