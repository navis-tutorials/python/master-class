# Boolean can decalared in two ways either True or False
# Booleans are case-sensitive

# not True = False
# not False = True

day = "Friday"
temperature = 30
raining = False

if (day == "Saturday" and temperature > 27) or not raining:
    print("Go swimming.")
else:
    print("Learn Python.")

# Check truthy values
print()
print("Truthy values:")

if 0:
    print("True")
else:
    print("False")

name = input("Please enter your name: ")
if name:
    print("Hello, {}.".format(name))
else:
    print("Are you the man with no name?")

# Another way to check the empty value
print()
print("Another way to check the empty string")
if name != "":
    print("Hello, {}.".format(name))
else:
    print("Are you the man with no name?")
